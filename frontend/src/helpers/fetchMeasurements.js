export const fetchMeasurements = async(smartplugId, timeFrame) => {
    //const url = `http://104.131.0.238:3000/today?smartplugID='${smartplugId}'`;
    const url = `http://104.131.0.238:3000/Data/${timeFrame}?smartplugID='${smartplugId}'`;
    //console.log(url);
    const resp = await fetch(url);
    const data = await resp.json();

    //console.log("fetch");
    
    return data;
}