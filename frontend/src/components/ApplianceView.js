import React, {useState} from 'react'
import { MeasurementPlot } from './MeasurementPlot';

export const ApplianceView = ({applianceId, handleBack}) => {

    const measurementTypes = ["power", "current", "voltage", "energy"];

    const [currentMeasurement, setCurremtMeasurement] = useState(measurementTypes[0])
    const [timeFrame, setTimeFrame] = useState("Hour");

    const handleMeasurementSelection = ( {target} ) => {
        const {name} = target;
        setCurremtMeasurement(name);
    }

    const handleTimeFrame = ({target}) => {
        const {name} = target;
        setTimeFrame(name);
    }

    return (
        <>
            <h1>Appliance {applianceId}</h1>

            { measurementTypes.map( (type) => (
                <button
                    className = "btn btn-primary"
                    onClick = {handleMeasurementSelection}
                    name = {type}
                    key = {type}
                >
                    {type}
                </button>
            )) }

            
            <hr />
            <h2>{currentMeasurement}</h2>

            <div className="d-flex mb-3">
                <button className="btn btn-primary me-auto" onClick = {handleBack}>Back</button>
                
                <button
                    className="btn btn-primary"
                    name = "Hour"
                    onClick = {handleTimeFrame}
                >
                    Hour
                </button>
                <button
                    className="btn btn-primary"
                    name = "Today"
                    onClick = {handleTimeFrame}
                >
                    Today
                </button>
                <button
                    className="btn btn-primary"
                    name = "All"
                    onClick = {handleTimeFrame}
                >
                    All
                </button>
            </div>
            
            <MeasurementPlot smartplugId={applianceId} timeFrame={timeFrame} measurement = {currentMeasurement} />

        </>
    )
}
