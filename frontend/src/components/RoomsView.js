import React, { useState } from 'react';
import {ApplianceView} from './ApplianceView';

export const RoomsView = () => {

    //const rooms = ["All", "Livingroom", "Bedroom"];
    const appliances = ['98:F4:AB:E2:D7:24', '98:F4:AB:E2:E9:DE', '98:F4:AB:E2:E8:B8', '98:F4:AB:E3:0A:DF', '98:F4:AB:E2:DC:3D'];

    const [currentView, setCurrentView] = useState("Rooms");

    const handleSelectApliance = ({target}) => {
        const {name} = target;
        setCurrentView(name);
    }

    const handleBack = () => {
        setCurrentView("Rooms");
    }

    return (
        <>
            { (currentView==="Rooms")?(<>
                <h1>Rooms</h1>
                <hr />
                <h2>All</h2>

                <ul>
                    { appliances.map( (appliance) => 
                    <li key = {appliance}>
                        <button 
                            className = "btn btn-outline-primary w-50"
                            name = {appliance}
                            onClick = {handleSelectApliance}
                        >
                            {appliance}
                        </button>
                    </li>) }
                </ul>
            </>):(
                <ApplianceView applianceId={currentView} handleBack={handleBack} />
            )}
            

            

        </>
    )
}
