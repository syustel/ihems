import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {Plot, /*fromFlux,*/ newTable} from '@influxdata/giraffe'
import { fetchMeasurements } from '../helpers/fetchMeasurements'

export const MeasurementPlot = ( {smartplugId, timeFrame, measurement} ) => {

    const [tableData, setTableData] = useState(newTable(0)
        .addColumn('_time', 'dateTime:RFC3339', 'time', [])
        .addColumn('_value', 'double', 'number', []));
    
    const [data, setData] = useState([]);

    const [loading, setLoading] = useState(true);

    // Actualiza los datos a graficar cada vez que se cambia el measurement o llega nueva data
    useEffect(() => {
        const length = data.length;
        const time = data.map( (m) => Date.parse(m.time) );
        const value = data.map( (m) => m[measurement] );
        setTableData(newTable(length)
            .addColumn('_time', 'dateTime:RFC3339', 'time', time)
            .addColumn('_value', 'double', 'number', value));
    }, [measurement, data]);

    // Carga inicial de datos
    useEffect(() => {
        fetchMeasurements(smartplugId, timeFrame)
        .then( (measurements) => {
            setData(measurements);
            setLoading(false);
        })
        .catch( (err) => {console.log(err)});
    }, [smartplugId, timeFrame]);

    // Actualizacion constante
    useEffect( () => {
        setLoading(true);
        const interval = setInterval(() => {
            fetchMeasurements(smartplugId, timeFrame)
            .then( (measurements) => {
                setData(measurements);
                setLoading(false);
            })
            .catch( (err) => {console.log(err)});
        }, 10000);
        
        return () => {
            clearInterval(interval);
        }

    }, [smartplugId, timeFrame]);
    

    /*
    const fluxResultCSV = `#datatype,string,long,dateTime:RFC3339,dateTime:RFC3339,dateTime:RFC3339,double,string,string,string,string
#group,false,false,true,true,false,false,true,true,true,true
#default,_result,,,,,,,,,
,result,table,_start,_stop,_time,_value,_field,_measurement,example,location
,,0,2020-03-25T20:58:15.731129Z,2020-04-24T20:58:15.731129Z,2020-04-03T18:31:33.95Z,29.9,value,temperature,index.html,browser
,,0,2020-03-25T20:58:15.731129Z,2020-04-24T20:58:15.731129Z,2020-04-03T18:55:23.863Z,28.7,value,temperature,index.html,browser
,,0,2020-03-25T20:58:15.731129Z,2020-04-24T20:58:15.731129Z,2020-04-03T19:50:52.357Z,15,value,temperature,index.html,browser
,,0,2020-03-25T20:58:15.731129Z,2020-04-24T20:58:15.731129Z,2020-04-03T19:53:37.198Z,24.8,value,temperature,index.html,browser
,,0,2020-03-25T20:58:15.731129Z,2020-04-24T20:58:15.731129Z,2020-04-03T19:53:53.033Z,23,value,temperature,index.html,browser
,,0,2020-03-25T20:58:15.731129Z,2020-04-24T20:58:15.731129Z,2020-04-03T20:19:21.88Z,20.1,value,temperature,index.html,browser
,,0,2020-03-25T20:58:15.731129Z,2020-04-24T20:58:15.731129Z,2020-04-10T22:20:40.776Z,28.7,value,temperature,index.html,browser
`;

    const dataFromFlux = fromFlux(fluxResultCSV)
*/
    const lineLayer = {
        type: "line",
        x: "_time",
        y: "_value",
        lineWidth: 6
    }

    //const data = newTable(3)
    //      .addColumn('_time', 'dateTime:RFC3339', 'time', [1589838401244, 1589838461244, 1589838521244])
    //      .addColumn('_value', 'double', 'number', [2.58, 7.11, 4.79]);

    const config = {
        table: tableData,
        layers: [lineLayer],
    }

    return (
        <>
            {loading && <p>loading...</p>}
            <Plot config = {config} />
        </>
    )
}

MeasurementPlot.propTypes = {
    measurement: PropTypes.string.isRequired
}