import React from 'react';
import ReactDOM from 'react-dom';
//import { ApplianceView } from './components/ApplianceView';
import { RoomsView } from './components/RoomsView';
import './index.css';

ReactDOM.render(
    //<ApplianceView />,
    <RoomsView />,
  document.getElementById('root')
);