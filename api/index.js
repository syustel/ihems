const Influx = require('influx');
const Express = require('express');
const app = Express();
const bodyParser = require('body-parser');
const dateFormat = require('dateformat');
const cors = require('cors');


app.use(bodyParser.json());
app.use(cors());

// Create new influxbd client

const influx = new Influx.InfluxDB({
	host: 'localhost',
	database: 'mqtt-telegraf',
	username: 'admin',
	password: 'ihems2021'
});


// Routes

app.get('/', (req, res) => {
	res.send('Hello home');
});

app.get('/Data/All', async (req, res) => {
	const smartplugID = req.query.smartplugID;
	try {
		const results = await influx.query(`select power,voltage,current,energy from mqtt_consumer_json_test where mac=${smartplugID}`);
		res.json(results);
	} catch(err) {
		console.log('error', err);
		res.json({message:err});
	}
});

app.get('/Data/Today', async (req, res) => {
	const smartplugID = req.query.smartplugID;
	const now = new Date();
	//console.log(now);
	//console.log();
	const today = dateFormat(now, 'yyyy-mm-dd');
	const query = `select power,voltage,current,energy from mqtt_consumer_json_test where mac=${smartplugID} and time >= '${today}'`;
	//console.log(query);
	try {
		const results = await influx.query(query);
		res.json(results);
	} catch(err) {
		console.log('error', err);
		res.json({message:err});
	}
});


app.get('/Data/Hour', async (req, res) => {
	const smartplugID = req.query.smartplugID;
	//const now = new Date();
	//console.log(now);
	//console.log();
	//const hour = dateFormat(now, 'yyyy-mm-dd hh:MM:ss');
	const query = `select power,voltage,current,energy from mqtt_consumer_json_test where mac=${smartplugID} and time >= now() - 5h`; //cambiar a 1h despues de telegraf
	//console.log(query);
	try {
		const results = await influx.query(query);
		res.json(results);
	} catch(err) {
		console.log('error', err);
		res.json({message:err});
	}
});


app.get('/databases', async (req, res) => {
	try {
		const results = await influx.getDatabaseNames();
		res.json(results);
	} catch(err) {
		console.log('error', err);
		res.json({message:err});
	}
});

app.listen(3000);
console.log('Api runing in port 3000');
